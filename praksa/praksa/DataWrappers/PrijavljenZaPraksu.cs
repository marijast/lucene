﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace praksa.DataWrappers
{
    public class PrijavljenZaPraksu
    {
        public int IdUser { get; set; }
        public int IdPost { get; set; }
    }
}
