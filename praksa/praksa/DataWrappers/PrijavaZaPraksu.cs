﻿using praksa.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace praksa.DataWrappers
{
    public class PrijavaZaPraksu
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public int IdPosta { get; set; }
    }
}
