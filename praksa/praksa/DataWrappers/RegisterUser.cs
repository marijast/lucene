﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace praksa.DataWrappers
{
    public class RegisterUser
    {
        public String Ime { get; set; }
        public String Prezime { get; set; }
        public String Username { get; set; }
        public String Password { get; set; }
        public String Email { get; set; }
    }
}
