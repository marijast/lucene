﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace praksa.DataWrappers
{
    public class Login
    {
        public String username { get; set; }
        public String password { get; set; }
    }
}
