﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace praksa.DTOs
{
    public class UserView
    {
        public int Id { get; set; }
        public String Ime { get; set; }
        public String Prezime { get; set; }
        public String Username { get; set; }
        public String Password { get; set; }
        public String Email { get; set; }


        public UserView(Entities.User k)
        {
            this.Id = k.Id;
            this.Ime = k.Ime;
            this.Prezime = k.Prezime;
            this.Username = k.Username;
            this.Password = k.Password;
            this.Email = k.Email;
        }
    }
}
