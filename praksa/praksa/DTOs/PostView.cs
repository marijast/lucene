﻿using praksa.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace praksa.DTOs
{
    public class PostView
    {
        public int? Id { get; set; }
        public String Naziv { get; set; }
        public String Opis { get; set; }
        public String Trajanje { get; set; }
        public String Grad { get; set; }

        public FirmaView Firma { get; set; }
        public  IList<String> PrijavljeniKandidati { get; set; }



        public PostView(Entities.Post k)
        {
            this.Id = k.Id;
            this.Naziv = k.Naziv;
            this.Opis = k.Opis;
            this.Trajanje = k.Trajanje;
            this.Grad = k.Grad;
            this.Firma = new FirmaView(k.PripadaFirmi);
            this.PrijavljeniKandidati = new List<String>();
            foreach (User user in k.PrijavljeniKandidati)
            {
                this.PrijavljeniKandidati.Add(user.Email);
            }
        }
    }
}
