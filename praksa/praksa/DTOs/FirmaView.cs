﻿using praksa.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace praksa.DTOs
{
    public class FirmaView
    {
        public int? Id { get; set; }
        public String Naziv { get; set; }
        public String Username { get; set; }
        public String Password { get; set; }
        public String Adresa { get; set; }
        public String Logo { get; set; }
        public String Link { get; set; }
        public String Email { get; set; }

        public IList<String> Oglasi { get; set; }



        public FirmaView(Entities.Firma firma)
        {
            this.Id = firma.Id;
            this.Naziv = firma.Naziv;
            this.Username = firma.Username;
            this.Password = firma.Password;
            this.Adresa = firma.Adresa;
            this.Logo = firma.Logo;
            this.Link = firma.Link;
            this.Email = firma.Email;

            this.Oglasi = new List<String>();
            foreach (Post oglas in firma.Oglasi)
            {
                this.Oglasi.Add(firma.Naziv);
            }
        }
    }
}
