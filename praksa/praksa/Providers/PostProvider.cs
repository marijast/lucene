﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NHibernate;
using praksa.DataWrappers;
using praksa.DTOs;
using praksa.Entities;

namespace praksa.Providers
{
    public class PostProvider
    {
        public IEnumerable<praksa.DTOs.PostView> GetPostView()
        {
            NHibernate.ISession s = DataLayer.GetSession();

            IEnumerable<DTOs.PostView> posts = s.Query<Post>()
                .Select(p => new DTOs.PostView(p));
            return posts;
        }
        public List<Post> GetPosts()
        {
            NHibernate.ISession s = DataLayer.GetSession();

            List<Post> posts = s.Query<Post>()
                .Select(p => p).ToList();
            return posts;
        }
        public Post GetPost(int id)
        {
            NHibernate.ISession s = DataLayer.GetSession();

            return s.Query<Post>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();
        }

        public bool AddPost(AddPost addPost)
        {
            try
            {
                ISession session = DataLayer.GetSession();

                Post post = new Post();
                post.Naziv = addPost.Naziv;
                post.Opis = addPost.Opis;
                post.Trajanje = addPost.Trajanje;
                post.Grad = addPost.Grad;

        
                Firma d = session.Query<Firma>()
                .Where(v => v == addPost.Firma).Select(p => p).FirstOrDefault();
                post.PripadaFirmi = d;
                session.Save(post);

          
                session.Flush();
                session.Close();

                return true;
            }
            catch (Exception ec)
            {
                return false;
            }
        }
        public bool AddPost(Post addPost)
        {
            try
            {
                ISession session = DataLayer.GetSession();
                session.Save(addPost);


                session.Flush();
                session.Close();

                return true;
            }
            catch (Exception ec)
            {
                return false;
            }
        }

        public List<Post> vratiSveOglase(Firma firma)
        {
            ISession s = DataLayer.GetSession();
            

            int id = s.Query<Firma>()
                .Where(v => v.Username.Equals(firma.Username))
                .Select(p => p.Id).FirstOrDefault();

            List<Post> oglasi = s.Query<Post>()
                .Where(v => v.PripadaFirmi.Id == id)
                .Select(p => p).ToList();


            return oglasi;
        }

        public List<Post> PretragaGrad(string grad)
        {
            ISession s = DataLayer.GetSession();

            List<Post> rezultatiPretrage = s.Query <Post> ()
                 .Where(p => p.Grad== grad).Select(p => p).ToList();

            return rezultatiPretrage;
        }

        public int ObrisiPost(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Post post = s.Query<Post>().Where(v => v.Id == id).Select(p => p).FirstOrDefault();

                s.Delete(post);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception exc)
            {
                return -1;
            }

        }

        public List<PostView> GetPostsViews(List<Post> posts)
        {
            List<PostView> postViews=new List<PostView>();
            if (posts != null)
            {
                foreach (Post p in posts)
                {
                    PostView pv = new PostView(p);
                    postViews.Add(pv);
                }
                return postViews;
            }
            return null;
        }

    }
}
