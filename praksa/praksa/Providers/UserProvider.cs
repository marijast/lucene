﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NHibernate;
using praksa.DataWrappers;
using praksa.Entities;
using MailKit.Net.Smtp;
using MailKit;
using MimeKit;

namespace praksa.Providers
{
    public class UserProvider
    {
        public IEnumerable<praksa.DTOs.UserView> GetUserView()
        {
            NHibernate.ISession s = DataLayer.GetSession();

            IEnumerable<DTOs.UserView> users = s.Query<User>()
                .Select(p => new DTOs.UserView(p));
            return users;
        }

        public List<User> GetUsers()
        {
            NHibernate.ISession s = DataLayer.GetSession();

            List<User> klijenti = s.Query<User>()
                .Select(p => p).ToList();
            return klijenti;
        }
        public User GetUser(int id)
        {
            NHibernate.ISession s = DataLayer.GetSession();

            return s.Query<User>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();
        }

        public int AddUser(User k)
        {
            try
            {
                NHibernate.ISession s = DataLayer.GetSession();

                /*  Zaposleni zap = s.Query<Zaposleni>()
                   .Where(v => v.Username == k.Username).Select(p => p).FirstOrDefault();*/

                User postoji = s.Query<User>()
                 .Where(v => v.Username == k.Username).Select(p => p).FirstOrDefault();

                if (postoji == null)
                {

                    s.Save(k);
                    s.Flush();
                }
                else
                {
                    return -1;// vec postoji
                }

                s.Close();

                return 1;
            }
            catch (Exception ec)
            {
                return -1;
            }

        }

        public bool Postoji(String username, String password)
        {
            try
            {
                NHibernate.ISession s = DataLayer.GetSession();
                User postoji = s.Query<User>()
                 .Where(v => v.Username.Equals(username) && v.Password.Equals(password))
                 .Select(p => p).FirstOrDefault();

                if (postoji != null)
                    return true; // sve tacno

                return false; //nista se ne poklapa
            }
            catch (Exception ec)
            {
                return false;
            }
        }

        public User GetUserByUsername(string username,string password)
        {
            NHibernate.ISession s = DataLayer.GetSession();

            return s.Query<User>()
                .Where(v => v.Username == username && v.Password==password).Select(p => p).FirstOrDefault();
        }
        public bool Firma(String username, String password)
        {
            ISession s = DataLayer.GetSession();
            Firma firma = s.Query<Firma>()
                .Where(v => v.Username == username && v.Password == password).Select(p => p).FirstOrDefault();
            if (firma != null)
            {
                return true;
            }

            return false;
        }

        public void Mail(User user, String poruka)
        {
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress("Uspesna prijava", "internshipservice@gmail.com"));
            message.To.Add(new MailboxAddress(user.Ime, user.Email));
            message.Subject = "Prijava za praksu";

            message.Body = new TextPart("plain")
            {
                Text =poruka + " \n S poštovanjem, \n Vaš Internship service!"
            };
            using (var client = new SmtpClient())
            {
                client.Connect("smtp.gmail.com", 587, false);
                client.Authenticate("internshipservice", "asdfghhgfdsa10");
                client.Send(message);
                client.Disconnect(true);
            }

        }
    }
}
