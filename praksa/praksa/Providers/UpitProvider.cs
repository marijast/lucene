﻿using praksa.Entities;
using System;
using NHibernate;
using praksa.DataWrappers;
using praksa.DTOs;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace praksa.Providers
{
    public class UpitProvider
    {
        public bool AddUpit(Upit upit)
        {
            try
            {
                ISession session = DataLayer.GetSession();
                session.Save(upit);


                session.Flush();
                session.Close();

                return true;
            }
            catch (Exception ec)
            {
                return false;
            }
        }
        public bool AddUpitDW(NoviUpit noviU)
        {
            try
            {
                ISession session = DataLayer.GetSession();

                Upit upit = new Upit();
                upit.Mail = noviU.Mail;
                upit.ZadatiUpit = noviU.ZadatiUpit;


                session.Save(upit);

           

                session.Flush();
                session.Close();

                return true;
            }
            catch (Exception ec)
            {
                return false;
            }
        }

    }
}
