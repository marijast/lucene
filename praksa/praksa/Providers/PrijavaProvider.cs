﻿using NHibernate;
using praksa.DataWrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace praksa.Providers
{
    public class PrijavaProvider
    {

        public bool PrijavljivanjeZaPraksu(int userId,int postId)
        {
            PrijavljenZaPraksu novi = new PrijavljenZaPraksu();
            novi.IdPost = postId;
            novi.IdUser = userId;
            try
            {
                ISession session = DataLayer.GetSession();
                session.Save(novi);


                session.Flush();
                session.Close();

                return true;
            }
            catch (Exception ec)
            {
                return false;
            }
        }
    }
}
