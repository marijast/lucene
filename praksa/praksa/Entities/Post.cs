﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace praksa.Entities
{
    public class Post
    {
        public virtual int Id { get; set; }
        public virtual string Naziv { get; set; }
        public virtual string Opis { get; set; }
        public virtual string Trajanje { get; set; }
        public virtual string Grad { get; set; }
        public virtual Firma PripadaFirmi { get; set; }
        public virtual IList<User> PrijavljeniKandidati { get; set; }

        public Post()
        {
            PrijavljeniKandidati = new List<User>();
        }


    }
}
