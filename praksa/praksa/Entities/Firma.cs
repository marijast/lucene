﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace praksa.Entities
{
    public class Firma
    {
        public virtual int Id { get; set; }
        public virtual string Naziv { get; set; }
        public virtual string Username { get; set; }
        public virtual string Password { get; set; }
        public virtual string Adresa { get; set; }
        public virtual string Logo { get; set; }
        public virtual string Link { get; set; }
        public virtual string Email { get; set; }
        public virtual IList<Post> Oglasi{ get; set; }
        public Firma()
        {
            Oglasi = new List<Post>();
        }
    }
}
