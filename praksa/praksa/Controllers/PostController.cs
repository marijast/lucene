﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using praksa.DataWrappers;
using praksa.DTOs;
using praksa.Entities;
using praksa.Providers;
using praksa.Search;

namespace praksa.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostController : ControllerBase
    {
        [HttpGet]
       public IEnumerable<DTOs.PostView> Get()
        {

            PostProvider provider = new PostProvider();
            IEnumerable<DTOs.PostView> posts = provider.GetPostView();

            return posts;
        }
        /*  public IEnumerable<Post> Get()
          {

              PostProvider provider = new PostProvider();
              IEnumerable<Post> posts = provider.GetPosts();

              return posts;
          }*/


        // GET api/<controller>/5
        [HttpGet("{id}")]
        public Post Get(int id)
        {
            PostProvider provider = new PostProvider();
            Post post = provider.GetPost(id);

            return post;
        }
     /*   [HttpGet("pretraga/{query}")]
       public List<Post> Pretraga(string query)
        {
            LuceneSearch search = new LuceneSearch();
            List<Post> rezultatiPretrage = new List<Post>();
            rezultatiPretrage= search.SearchOnIndex(query, "Opis");

            return rezultatiPretrage;
        }*/
        [HttpGet("pretraga/{query}")]

        public List<PostView> Pretraga(string query)
        {
            LuceneSearch search = new LuceneSearch();
            List<Post> rezultatiPretrage = new List<Post>();
            rezultatiPretrage = search.SearchOnIndex(query, "Opis");
            PostProvider postProvider = new PostProvider();

            List<PostView> rezultat = new List<PostView>();
            rezultat= postProvider.GetPostsViews(rezultatiPretrage);
            return rezultat;
        }
        [HttpGet("pretragaGrad/{grad}")]
        public List<PostView> PretragaGrad(string grad)
        {
            PostProvider provider = new PostProvider();
            List<Post> rezultatiPretrage = new List<Post>();
            rezultatiPretrage = provider.PretragaGrad(grad);

            List<PostView> rezultat = new List<PostView>();
            rezultat = provider.GetPostsViews(rezultatiPretrage);
            return rezultat;
        }
        [HttpGet("naprednaPretraga/{query}")]

        public List<PostView> naprednaPretraga(string query)
        {
            LuceneSearch search = new LuceneSearch();
            List<Post> rezultatiPretrage = new List<Post>();
            string[] fields = { "Grad","Trajanje" };
            rezultatiPretrage = search.SearchOnIndexMulti(query, fields);
            PostProvider postProvider = new PostProvider();

            List<PostView> rezultat = new List<PostView>();
            rezultat = postProvider.GetPostsViews(rezultatiPretrage);
            return rezultat;
        }
        [HttpPost]
        [Route("naprednaPretraga1")]
        public List<PostView> naprednaPretraga1(Pretraga parametri)
        {
            LuceneSearch search = new LuceneSearch();
            List<Post> rezultatiPretrage = new List<Post>();

            rezultatiPretrage = search.SearchOnIndex3(parametri, "Napredna");
            PostProvider postProvider = new PostProvider();

            List<PostView> rezultat = new List<PostView>();
            rezultat = postProvider.GetPostsViews(rezultatiPretrage);
            return rezultat;
        }
        /*   [HttpGet("pretragaGrad/{grad}")]
           public List<Post> PretragaGrad(string grad)
           {
               PostProvider provider = new PostProvider();
               List<Post> rezultatiPretrage = new List<Post>();
               rezultatiPretrage = provider.PretragaGrad(grad);
           return rezultatiPretrage;
           }*/
        [HttpPost]
        [Route("addPost")]
        public IActionResult Post([FromBody] AddPost addPost )
        {
            PostProvider provider = new PostProvider();
            FirmaProvider firmaProvider = new FirmaProvider();
            Post post = new Post();
            post.Naziv = addPost.Naziv;
            post.Trajanje = addPost.Trajanje;
            post.Grad = addPost.Grad;
            post.Opis = addPost.Opis;
            post.PripadaFirmi = addPost.Firma;

          
            bool po = provider.AddPost(post);
            if (po)
            {
                IndexPost indexPost = new IndexPost();
                indexPost.Id = post.Id;
                indexPost.Naziv = post.Naziv;
                indexPost.Opis = post.Opis;
                indexPost.Grad = post.Grad;
                indexPost.Trajanje = post.Trajanje;
                LuceneSearch index = new LuceneSearch();
                index.CreateIndex(indexPost);
                var tip = new { tip = "dodato" };
                return Ok(tip);
            }
            return NotFound();
        }
        [HttpOptions]
        [Route("addPost")]

        public IActionResult PostCorseCheck()
        {
            return Ok();
        }

        [HttpDelete("{id}")]
        public int Delete(int id)
        {
            LuceneSearch delIndex = new LuceneSearch();
            bool deleted=delIndex.DeleteIndexed(id);
            if (deleted)
            {
                PostProvider provider = new PostProvider();
                return provider.ObrisiPost(id);
            }
            else 
            {
                return 0;
            }
        }
    }


}
