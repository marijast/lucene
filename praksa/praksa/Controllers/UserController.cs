﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using praksa.DataWrappers;
using praksa.Entities;
using praksa.Providers;



// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace praksa.Controllers
{
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        // GET: api/<controller>
        [HttpGet]
        public IEnumerable<DTOs.UserView> Get()
        {

            UserProvider provider = new UserProvider();
            IEnumerable<DTOs.UserView> posts = provider.GetUserView();

            return posts;
        }
        /* public IEnumerable<User> Get()
         {

             UserProvider provider = new UserProvider();
             IEnumerable<User> klijenti = provider.GetUsers();

             return klijenti;
         }
        */
        // GET api/<controller>/5
        [HttpGet("{id}")]
        public User Get(int id)
        {
            UserProvider provider = new UserProvider();
            User u = provider.GetUser(id);
            return u;
        }

        // POST api/<controller>
      /*  [HttpPost]
        [Route("LogIn")]
        public IActionResult Prijavljivanje([FromBody] RegisterUser prijava)
        {
            UserProvider provider = new UserProvider();

            /*  if (provider.Zaposleni(prijava.Username, prijava.Password))
              {
                  var tip = new { tip = "Zaposleni" };
                  return Ok(tip);
              }

            if (provider.Postoji(prijava.Username))
            {
                var tip = new { tip = "Klijent" };
                return Ok(tip);
            }
            return NotFound();
        }*/

        [HttpPost]
        [Route("LogIn")]
        public IActionResult Prijavljivanje([FromBody] Login prijava)
        {
            UserProvider provider = new UserProvider();

            if (provider.Firma(prijava.username, prijava.password))
            {
                var tip = new { tip = "Firma" };
                return Ok(tip);
            }

            if (provider.Postoji(prijava.username, prijava.password))
            {
                var tip = new { tip = "Klijent" };
                return Ok(tip);
            }
            return NotFound();
        }
        [HttpOptions]
        [Route("LogIn")]

        public IActionResult PostCorseCheck()
        {
            return Ok();
        }

        [HttpPost]
        [Route("registrujSe")]
        public IActionResult RegistrujSe([FromBody] RegisterUser prijava)
        {
            UserProvider provider = new UserProvider();
        
            if (provider.Postoji(prijava.Username,prijava.Password) || provider.Firma(prijava.Username,prijava.Password) )
            {
                var tip = new { tip = "Postoji" };
                return Ok(tip);
            }

            User k = new User();
            k.Ime = prijava.Ime;
            k.Username = prijava.Username;
            k.Prezime = prijava.Prezime;
            k.Password = prijava.Password;
            k.Email = prijava.Email;



            int result = provider.AddUser(k);

           
          
            if (result == 1)
            {
                var tip = new { tip = "Klijent" };
                return Ok(tip);
            }

            return NotFound();

        }

        [HttpOptions]
        [Route("registrujSe")]

        public IActionResult PostCorseCheck1()
        {
            return Ok();
        }

        [HttpGet("byUsername/{username}")]

        public IActionResult GetUserByUsername(string username)
        {
            UserProvider provider = new UserProvider();
            User user = provider.GetUsers().Where<User>(x => x.Username == username).FirstOrDefault();
            var result = new
            {
                Username = user.Username,
                Password = user.Password
            };
            return Ok(result);
        }
        [HttpPost]
        [Route("prijavaZaPraksu")]
        public IActionResult PrijaviSeZaPraksu([FromBody] PrijavaZaPraksu prijava) //id Smestaja
        {
            FirmaProvider firmaProvider = new FirmaProvider();
           // Firma firma = firmaProvider.VratiFirmuCijiJePostId(prijava.IdPosta);*/
            PostProvider postProvider = new PostProvider();
            Post post = postProvider.GetPost(prijava.IdPosta);
            Firma firma = new Firma();
            firma = post.PripadaFirmi;
            UserProvider klijentProvider = new UserProvider();

            if (klijentProvider.Postoji(prijava.Username, prijava.Password))
            {
                User klijent = klijentProvider.GetUserByUsername(prijava.Username, prijava.Password);
                //PrijavaProvider prijavaProvider = new PrijavaProvider();
                //  prijavaProvider.PrijavljivanjeZaPraksu(klijent.Id,prijava.IdPosta);
                klijent.PrijavljenZaOglase.Add(post);
                post.PrijavljeniKandidati.Add(klijent);
                string poruka = "Korisnik cija je email adresa: " + klijent.Email + " zeli da se prijavi za praksu u vasoj firmi. ";
                firmaProvider.Mail(firma, poruka);
                string porukaUser = "Uspesno ste se prijavili za praksu u firmi " + firma.Naziv +". \n Posetite sajt firme: " + firma.Link ;
                klijentProvider.Mail(klijent, porukaUser);
                var tip = new { tip = "Uspesna prijava" };
                return Ok(tip);
            }
            else
            {
                var tip = new { tip = "Nije ulogovan korisnik" };
                return Ok(tip);
            }
        }
        /*[HttpPost]
        [Route("prijavaZaPraksu")]
        public IActionResult PrijaviSeZaPraksu([FromBody] PrijavaZaPraksu prijava) //id Smestaja
        {
            FirmaProvider firmaProvider = new FirmaProvider();
            Firma firma=firmaProvider.VratiFirmuCijiJePostId(prijava.IdPosta);
            PostProvider postProvider = new PostProvider();
            Post post = postProvider.GetPost(prijava.IdPosta);
            UserProvider klijentProvider = new UserProvider();

            if (klijentProvider.Postoji(prijava.Username, prijava.Password))
            {
                User klijent= klijentProvider.GetUserByUsername(prijava.Username, prijava.Password);
                //PrijavaProvider prijavaProvider = new PrijavaProvider();
                //  prijavaProvider.PrijavljivanjeZaPraksu(klijent.Id,prijava.IdPosta);
                klijent.PrijavljenZaOglase.Add(post);
                post.PrijavljeniKandidati.Add(klijent);
                string poruka = "Korisnik cija je email adresa: " + klijent.Email + " zeli da se prijavi za praksu u vasoj firmi. ";
                firmaProvider.Mail(firma, poruka);
                var tip = new { tip = "Uspesna prijava" };
                return Ok(tip);
            }
            else
            {
                var tip = new { tip = "Nije ulogovan korisnik" };
                return Ok(tip);
            }
        }*/


        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
