﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using praksa.Entities;


namespace praksa.Mapping
{
    public class PostMapping : ClassMap<Post>
    {
        public PostMapping()
        {
            Table("oglas");

            Id(x => x.Id, "Id").GeneratedBy.Increment();

            Map(x => x.Naziv, "Naziv");
            Map(x => x.Trajanje, "Trajanje");
            Map(x => x.Grad, "Grad");
            Map(x => x.Opis, "Opis");

            References(x => x.PripadaFirmi).Column("idFirme");

            HasManyToMany(x => x.PrijavljeniKandidati)
               .Table("prijavljivanjepraksa")
               .ParentKeyColumn("oglasId")
               .ChildKeyColumn("userId")
               .Inverse()
               .Cascade.All();

          // HasMany(x => x.PrijavljeniKandidati).KeyColumn("oglasId").LazyLoad().Cascade.All().Inverse();



        }
    }
}
