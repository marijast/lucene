﻿using FluentNHibernate.Mapping;
using praksa.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace praksa.Mapping
{
    public class FirmaMapping : ClassMap<Firma>
    { 
        public FirmaMapping()
        {
            Table("firma");

            Id(x => x.Id, "Id").GeneratedBy.Increment();

            Map(x => x.Naziv, "Naziv");
            Map(x => x.Username, "Username");
            Map(x => x.Password, "Password");
            Map(x => x.Adresa, "Adresa");
            Map(x => x.Logo, "Logo");
            Map(x => x.Link, "Link");
            Map(x => x.Email, "Email");

            HasMany(x => x.Oglasi).KeyColumn("idFirme").LazyLoad().Cascade.All().Inverse();


        }
    }
}
