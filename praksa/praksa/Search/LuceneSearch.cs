﻿using Lucene.Net.Util;
using Lucene.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Index;
using Lucene.Net.Store;
using Lucene.Net.Documents;
using Lucene.Net.Search;
using Lucene.Net.QueryParsers.Classic;
using praksa.DataWrappers;
using praksa.Entities;
using praksa.Providers;
using System.IO;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Remotion.Linq.Clauses.Expressions;

namespace praksa.Search
{
    public class LuceneSearch
    {
        public bool CreateIndex(IndexPost post)
        {
            var AppLuceneVersion = LuceneVersion.LUCENE_48;

            var indexLocation = @"F:\4 godina\indeksi";
                var dir = FSDirectory.Open(indexLocation);

                var analyzer = new StandardAnalyzer(AppLuceneVersion);

                var indexConfig = new IndexWriterConfig(AppLuceneVersion, analyzer);
                var writer = new IndexWriter(dir, indexConfig);
                Document doc = new Document
                {
            new TextField("Opis",
                post.Opis,
                Field.Store.YES){ Boost=5.0f},
            new TextField("Grad",
                post.Grad,
                Field.Store.YES){ Boost=5.0f},
            new TextField("Trajanje",
                post.Trajanje,
                Field.Store.YES){ Boost=5.0f},
    
            /* new TextField("Napredna",
                 post.Grad + post.Trajanje  ,
                Field.Store.YES){ Boost=3.0f},*/
            new TextField("Napredna",
                 "\"" + post.Grad + " " + post.Trajanje  + "\"",
                Field.Store.YES){ Boost=3.0f},
            new TextField("IdBrisanje",
                post.Id.ToString(),
                Field.Store.YES),
            new Int32Field("Id",
                post.Id,
                Field.Store.YES)
            };

                writer.AddDocument(doc);
                writer.Flush(triggerMerge: false, applyAllDeletes: false);
            // writer.commit();
            writer.Commit();

            IndexWriter.Unlock(dir);
                return true;
        
        }

        public List<Post> SearchOnIndex1(string query, string targetFieldName)
        {
            List<Post> posts = new List<Post>();
            var AppLuceneVersion = LuceneVersion.LUCENE_48;

            var indexLocation = @"F:\4 godina\indeksi";
            var dir = FSDirectory.Open(indexLocation);
            try
            {
                dir.ClearLock(indexLocation);

                var analyzer = new StandardAnalyzer(AppLuceneVersion);

                var indexConfig = new IndexWriterConfig(AppLuceneVersion, analyzer);
                var writer = new IndexWriter(dir, indexConfig);
                var searcher = new IndexSearcher(writer.GetReader(applyAllDeletes: true));

               
                {
                    var phrase = new PhraseQuery
                    {
                        new Term(targetFieldName,query),
                    };
                     var parsedQuery = new QueryParser(AppLuceneVersion, targetFieldName, analyzer).Parse(query);
                  //var parsedQuery = new TermQuery(phrase).parse;

                    int hitsPerPage = 20;

                    var collector = TopScoreDocCollector.Create(hitsPerPage, true);
                   // var hits = searcher.Search(phrase, 20).ScoreDocs;
                   // foreach (var hit in hits)
                   // {
                     //   var foundDoc = searcher.Doc(hit.Doc);
                   //     searcher.Search(phrase, collector);

                    var matches = collector.GetTopDocs().ScoreDocs;
                    PostProvider provider = new PostProvider();
                    for (int i = 0; i < matches.Length; ++i)
                    {
                        int docId = matches[i].Doc; // internal document id

                        var document = searcher.Doc(docId);
                        int idm = (int)document.GetField("Id").GetInt32Value();
                        Post post = new Post();
                        post.Opis = document.GetField("Opis").GetStringValue();
                        Post izBaze = provider.GetPost(idm);
                        post.Grad = izBaze.Grad;
                        post.Naziv = izBaze.Naziv;
                        post.Trajanje = izBaze.Trajanje;
                        post.Id = idm;
                        post.PripadaFirmi = izBaze.PripadaFirmi;
                        posts.Add(post);
                    }
                }

                IndexWriter.Unlock(dir);
                return posts;
            }
            catch
            {
                IndexWriter.Unlock(dir);
                return null;
            }

            finally
            {
                IndexWriter.Unlock(dir);
            }
        }



        public List<Post> SearchOnIndex(string query, string targetFieldName)
        {
            List<Post> posts = new List<Post>();
            var AppLuceneVersion = LuceneVersion.LUCENE_48;

            var indexLocation = @"F:\4 godina\indeksi";
            var dir = FSDirectory.Open(indexLocation);
            try
            {
                dir.ClearLock(indexLocation);

                var analyzer = new StandardAnalyzer(AppLuceneVersion);

                var indexConfig = new IndexWriterConfig(AppLuceneVersion, analyzer);
                var writer = new IndexWriter(dir, indexConfig);
                var searcher = new IndexSearcher(writer.GetReader(applyAllDeletes: true));

                {
               
                    var parsedQuery = new QueryParser(AppLuceneVersion, targetFieldName, analyzer).Parse(query);


                    int hitsPerPage = 20;

                    var collector = TopScoreDocCollector.Create(hitsPerPage, true);

                    searcher.Search(parsedQuery, collector);

                    var matches = collector.GetTopDocs().ScoreDocs;
                    PostProvider provider = new PostProvider();
                    for (int i = 0; i < matches.Length; ++i)
                    {
                        int docId = matches[i].Doc; // internal document id

                        var document = searcher.Doc(docId);
                        int idm = (int)document.GetField("Id").GetInt32Value();
                         Post post = new Post();
                         post.Opis = document.GetField("Opis").GetStringValue();
                         Post izBaze = provider.GetPost(idm);
                         post.Grad = izBaze.Grad;
                         post.Naziv = izBaze.Naziv;
                         post.Trajanje = izBaze.Trajanje;
                         post.Id =idm;
                         post.PripadaFirmi = izBaze.PripadaFirmi;
                         posts.Add(post);
                    }
                }

                IndexWriter.Unlock(dir);
                return posts;
            }
            catch
            {
                IndexWriter.Unlock(dir);
                return null;
            }

            finally
            {
                IndexWriter.Unlock(dir);
            }
        }
        public List<Post> SearchOnIndex2(string query, string targetFieldName)
        {
            List<Post> posts = new List<Post>();
            var AppLuceneVersion = LuceneVersion.LUCENE_48;

            var indexLocation = @"F:\4 godina\indeksi";
            var dir = FSDirectory.Open(indexLocation);
            try
            {
                dir.ClearLock(indexLocation);

                var analyzer = new StandardAnalyzer(AppLuceneVersion);

                var indexConfig = new IndexWriterConfig(AppLuceneVersion, analyzer);
                var writer = new IndexWriter(dir, indexConfig);
                var searcher = new IndexSearcher(writer.GetReader(applyAllDeletes: true));

                {

                    var parsedQuery = new QueryParser(AppLuceneVersion, targetFieldName, analyzer).Parse(query);


                    int hitsPerPage = 20;

                    var collector = TopScoreDocCollector.Create(hitsPerPage, true);

                    searcher.Search(parsedQuery, collector);

                    var matches = collector.GetTopDocs().ScoreDocs;
                    PostProvider provider = new PostProvider();
                    for (int i = 0; i < matches.Length; ++i)
                    {
                        int docId = matches[i].Doc; // internal document id

                        var document = searcher.Doc(docId);
                        int idm = (int)document.GetField("Id").GetInt32Value();
                        Post post = new Post();
                        post.Opis = document.GetField("Opis").GetStringValue();
                        Post izBaze = provider.GetPost(idm);
                        post.Grad = izBaze.Grad;
                        post.Naziv = izBaze.Naziv;
                        post.Trajanje = izBaze.Trajanje;
                        post.Id = idm;
                        post.PripadaFirmi = izBaze.PripadaFirmi;
                        posts.Add(post);
                    }
                }

                IndexWriter.Unlock(dir);
                return posts;
            }
            catch
            {
                IndexWriter.Unlock(dir);
                return null;
            }

            finally
            {
                IndexWriter.Unlock(dir);
            }
        }
        public List<Post> SearchOnIndex3(Pretraga parametri, string targetFieldName)
        {
            List<Post> posts = new List<Post>();
            var AppLuceneVersion = LuceneVersion.LUCENE_48;

            var indexLocation = @"F:\4 godina\indeksi";
            var dir = FSDirectory.Open(indexLocation);
            try
            {
                dir.ClearLock(indexLocation);

                var analyzer = new StandardAnalyzer(AppLuceneVersion);

                var indexConfig = new IndexWriterConfig(AppLuceneVersion, analyzer);
                var writer = new IndexWriter(dir, indexConfig);
                var searcher = new IndexSearcher(writer.GetReader(applyAllDeletes: true));

                {

                    // var parsedQuery = new QueryParser(AppLuceneVersion, targetFieldName, analyzer).Parse(query);
                    var phrase = new PhraseQuery
                    {
                        new Term(targetFieldName, parametri.grad),
                        new Term(targetFieldName, parametri.trajanje)
                    };

                    var phrase1 = new BooleanQuery();
                   // phrase1.Add(phrase,);
                    int hitsPerPage = 20;

                    var collector = TopScoreDocCollector.Create(hitsPerPage, true);

                    searcher.Search(phrase, collector);

                    var matches = collector.GetTopDocs().ScoreDocs;
                    PostProvider provider = new PostProvider();
                    for (int i = 0; i < matches.Length; ++i)
                    {
                        int docId = matches[i].Doc; // internal document id

                        var document = searcher.Doc(docId);
                        int idm = (int)document.GetField("Id").GetInt32Value();
                        Post post = new Post();
                        post.Opis = document.GetField("Opis").GetStringValue();
                        Post izBaze = provider.GetPost(idm);
                        post.Grad = izBaze.Grad;
                        post.Naziv = izBaze.Naziv;
                        post.Trajanje = izBaze.Trajanje;
                        post.Id = idm;
                        post.PripadaFirmi = izBaze.PripadaFirmi;
                        posts.Add(post);
                    }
                }

                IndexWriter.Unlock(dir);
                return posts;
            }
            catch
            {
                IndexWriter.Unlock(dir);
                return null;
            }

            finally
            {
                IndexWriter.Unlock(dir);
            }
        }

        public List<Post> SearchOnIndexMulti(string query, string[] targetFields)
        {
            List<Post> posts = new List<Post>();
            var AppLuceneVersion = LuceneVersion.LUCENE_48;

            var indexLocation = @"F:\4 godina\indeksi";
            var dir = FSDirectory.Open(indexLocation);
            try
            {
                dir.ClearLock(indexLocation);

                var analyzer = new StandardAnalyzer(AppLuceneVersion);

                var indexConfig = new IndexWriterConfig(AppLuceneVersion, analyzer);
                var writer = new IndexWriter(dir, indexConfig);
                var searcher = new IndexSearcher(writer.GetReader(applyAllDeletes: true));

                {
                    Operator AND_OPERATOR = Operator.AND;
                    MultiFieldQueryParser queryParser = new  MultiFieldQueryParser(AppLuceneVersion, targetFields, analyzer);
                    queryParser.DefaultOperator = Operator.AND;

                    // var parsedQuery = new QueryParser(AppLuceneVersion, targetFieldName, analyzer).Parse(query);
                    var parsedQuery = queryParser.Parse(query);
                   

                    int hitsPerPage = 20;

                    var collector = TopScoreDocCollector.Create(hitsPerPage, true);

                    searcher.Search(parsedQuery, collector);

                    var matches = collector.GetTopDocs().ScoreDocs;
                    PostProvider provider = new PostProvider();
                    for (int i = 0; i < matches.Length; ++i)
                    {
                        int docId = matches[i].Doc; // internal document id

                        var document = searcher.Doc(docId);
                        int idm = (int)document.GetField("Id").GetInt32Value();
                        Post post = new Post();
                        post.Opis = document.GetField("Opis").GetStringValue();
                        Post izBaze = provider.GetPost(idm);
                        post.Grad = izBaze.Grad;
                        post.Naziv = izBaze.Naziv;
                        post.Trajanje = izBaze.Trajanje;
                        post.Id = idm;
                        post.PripadaFirmi = izBaze.PripadaFirmi;
                        posts.Add(post);
                    }
                }

                IndexWriter.Unlock(dir);
                return posts;
            }
            catch
            {
                IndexWriter.Unlock(dir);
                return null;
            }

            finally
            {
                IndexWriter.Unlock(dir);
            }
        }


        public bool DeleteIndexed(int id)
        {
            var AppLuceneVersion = LuceneVersion.LUCENE_48;

            var indexLocation = @"F:\4 godina\indeksi";
            var dir = FSDirectory.Open(indexLocation);
            try
            {
                dir.ClearLock(indexLocation);

                var analyzer = new StandardAnalyzer(AppLuceneVersion);

                var indexConfig = new IndexWriterConfig(AppLuceneVersion, analyzer);
                var writer = new IndexWriter(dir, indexConfig);
                var searcher = new IndexSearcher(writer.GetReader(applyAllDeletes: true));
                var deleteQuery = new QueryParser(AppLuceneVersion, "IdBrisanje", analyzer).Parse(id.ToString());
                writer.DeleteDocuments(deleteQuery);
                writer.Flush(triggerMerge: false, applyAllDeletes: false);
                writer.Commit();

                IndexWriter.Unlock(dir);
                return true;
            }

            catch
            {
                IndexWriter.Unlock(dir);
                return false;
            }
            finally
            {
                IndexWriter.Unlock(dir);
            }
        }

    }
}
